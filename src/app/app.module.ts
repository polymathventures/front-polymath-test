import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginService } from './views/full-pages/login/services/login.service';
import { LoginComponent } from './views/full-pages/login/login.component';
import { TokenInterceptor } from './shared/interceptors/token.interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { TaskComponent } from './views/content-pages/notes/task.component';
import { CommonModule } from '@angular/common';
import { TaskService } from './views/content-pages/notes/services/task.service';
import { UserComponent } from './views/full-pages/user/user.component';
import { UserService } from './views/full-pages/user/services/user.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AuthGuard } from './shared/guards/auth.guard.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TaskComponent,
    UserComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgxDatatableModule,
    AppRoutingModule
  ],
  providers: [
    LoginService,
    TaskService,
    UserService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
