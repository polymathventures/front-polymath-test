// Angular
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
// RxJS
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  intercept(
		request: HttpRequest<any>,
		next: HttpHandler
	): Observable<HttpEvent<any>> {
		if (request.headers.get('No-Auth') === 'True') {
            return next.handle(request.clone());
		} else {
			if (localStorage.getItem(environment.authTokenKey) != null){
				request = request.clone({
					setHeaders: {
						Authorization: `Bearer ${localStorage.getItem(environment.authTokenKey)}`
					}
				});
			}
		}

		return next.handle(request).pipe(
			tap(
				event => {
					 if (event instanceof HttpResponse) {
						// console.log('Todo bien');
						// console.log(event.status);
					}
				},
				error => {
					console.error(error.status);
					console.error(error.message);
				}
			)
		);
	}
}
