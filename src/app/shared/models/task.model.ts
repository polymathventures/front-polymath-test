import { User } from "./user.model";

export class Task {
    taskId: number;
    user: User;
    description: string;
    status: number;
}