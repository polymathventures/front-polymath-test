import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Token } from '../../../../shared/models/token.model';

import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';

const API_LOGIN_URL = environment.api_url + 'login';

@Injectable()
export class LoginService {
  constructor(private http: HttpClient) {}

  login(userName: string, password: string): Observable<Token> {
      const httpHeaders = new HttpHeaders();
      httpHeaders.set('Content-Type', 'application/json');
      httpHeaders.set('No-Auth', 'True');

      var formData = { "username": userName, "password": password };

      return this.http.post<Token>(API_LOGIN_URL, formData, { headers: httpHeaders });
  }
}
