import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { LoginService } from './services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  title = 'front-polymath-test';
  username: string;
  password: string;

  constructor(
    private router: Router,
    private loginService: LoginService,
  ) {}

  ngOnInit() {
  }

  signin() {
    this.loginService
      .login(this.username, this.password).subscribe(
        mResponse => {
          if (mResponse) {
            localStorage.setItem(environment.idUserKey, mResponse.userId);
            localStorage.setItem(environment.authTokenKey, mResponse.token);
            this.router.navigateByUrl('/task');
          } else {
            alert("Error de inicio de sesión");
          }
        },
        mError => {
          alert("Error de inicio de sesión");
        }
      );
  }
}
