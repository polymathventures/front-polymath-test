import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { User } from 'src/app/shared/models/user.model';

const API_USER_URL = environment.api_url + 'user/create';

@Injectable()
export class UserService {
  constructor(private http: HttpClient) { }

  createUser(user: User): Observable<User> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('No-Auth', 'True');

    return this.http.post<User>(API_USER_URL, user, { headers: httpHeaders });
  }
}
