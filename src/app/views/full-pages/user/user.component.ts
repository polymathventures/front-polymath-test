import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/shared/models/user.model';
import { environment } from 'src/environments/environment';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  mUser: User;

  constructor(
    private router: Router,
    private userService: UserService,
  ) {}

  ngOnInit() {
      this.mUser = new User();
      this.mUser.status = 1;
  }

  addUser() {
    this.userService.createUser(this.mUser).subscribe(mResponse => {
        if (mResponse) {
            alert("Registro exitoso");
            this.router.navigateByUrl('/login');
        }
    });
  }

  back() {
    this.router.navigateByUrl('/login');
  }
}
