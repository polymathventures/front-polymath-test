import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/shared/models/task.model';
import { User } from 'src/app/shared/models/user.model';
import { environment } from 'src/environments/environment';
import { TaskService } from './services/task.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { StringMap } from '@angular/compiler/src/compiler_facade_interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  public ColumnMode = ColumnMode;

  description: string;
  descriptionFilter: string;
  catDescriptions: Task[] = [];
  page = {
    limit: 10,
    count: 0,
    offset: 0
  };

  constructor(private router: Router, private taskService: TaskService) { }

  ngOnInit() {
    this.pageCallback({ offset: 0 });
  }

  pageCallback(pageInfo: { count?: number, pageSize?: number, limit?: number, offset?: number }) {
    this.page.offset = pageInfo.offset;
    this.getDescription();
  }

  getDescription() {
    this.taskService.getTasks(localStorage.getItem(environment.idUserKey), this.descriptionFilter, this.page.offset, this.page.limit).subscribe(mResponse => {
      this.catDescriptions = mResponse;
      if (mResponse.length > 0){
        this.taskService.getTasksCount(localStorage.getItem(environment.idUserKey), this.descriptionFilter).subscribe(mResponse => {
          this.page.count = mResponse;
          this.catDescriptions = [...this.catDescriptions];
        });
      } else {
        this.catDescriptions = [];
        this.page.count = 0;
      };
    });
  }

  addDescription() {
    if (this.description?.length > 0) {
      let myTask = new Task();
      myTask.user = new User();
      myTask.user.userId = Number(localStorage.getItem(environment.idUserKey));
      myTask.description = this.description;
      myTask.status = 0;

      this.taskService.addTask(myTask).subscribe(mResponse => {
        this.catDescriptions.push(mResponse);
        this.catDescriptions = [...this.catDescriptions];
        this.page.count++;
      });
    }
  }

  deleteDescription(taskId: number) {
    this.taskService.deleteTask(taskId).subscribe(mResponse => {
      const targetIdx = this.catDescriptions.map(item => item.taskId).indexOf(taskId);
      this.catDescriptions.splice(targetIdx, 1);
      this.page.count--;
      this.catDescriptions = [...this.catDescriptions];
    });
  }

  updateDescription(taskId: number) {
    this.taskService.updateTask(taskId).subscribe(mResponse => {
      const targetIdx = this.catDescriptions.map(item => item.taskId).indexOf(taskId);
      this.catDescriptions[targetIdx] = mResponse;
      this.catDescriptions = [...this.catDescriptions];
    });
  }

  logOut() {
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }
}
