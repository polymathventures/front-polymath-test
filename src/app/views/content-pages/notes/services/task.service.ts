import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { Task } from 'src/app/shared/models/task.model';

const API_TASK_URL = environment.api_url + 'task/';

@Injectable()
export class TaskService {
  constructor(private http: HttpClient) { }

  getTasks(userId: string, description: string, pageNumber: number, pageSize: number): Observable<Task[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    const url = `${API_TASK_URL}${userId}/userId/${pageNumber}/page/${pageSize}/elements`;

    let body = {
      description: description?.length <= 0 ? null : description
    };

    return this.http.post<Task[]>(url, body, { headers: httpHeaders });
  }

  getTasksCount(userId: string, description: string,): Observable<number> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    const url = `${API_TASK_URL}count/${userId}/userId`;

    let body = {
      description: description?.length <= 0 ? null : description
    };

    return this.http.post<number>(url, body, { headers: httpHeaders });
  }

  addTask(task: Task): Observable<Task> {
    return this.http.post<Task>(API_TASK_URL, task);
  }

  deleteTask(taskId: number): Observable<Task> {
    return this.http.delete<Task>(API_TASK_URL + taskId);
  }

  updateTask(taskId: number): Observable<Task> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');

    return this.http.put<Task>(API_TASK_URL + taskId + '/status', null, { headers: httpHeaders} );
  }
}
